<?php

// Blog Header.
$blog_title = "tinymd Official Blog";
$blog_subtitle = "Everything tinymd";
$blog_author = "Nathan Campos";


// Blog Theme.
$blog_theme_name = "default";


// Blog Technical.
$blog_post_sort = SORT_DESC;  // Change this to SORT_ASC if your posts are getting displayed from the oldest to the newest.
$blog_post_limit = 10;  // Limits the number of posts on the front page.


// Disqus (comment system)
$enable_disqus = false;
$disqus_shortname = "";


// Trackers
$enable_analytics = true;
$analytics_tracking_id = "UA-35946529-1";

$enable_gauges = true;
$gauges_tracking_id = "508eaa9b613f5d76c2000053";

?>