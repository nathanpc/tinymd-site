# Updates (November 2, 2012)

So today's update is extremely tiny. All I did was implement the design for quotes (`<blockquotes>`), so now you can do things like this:

 > This is a quote

I still want to update that design a bit: Instead of having all that background, it'll be just sightly different than the post background, the text color will be a little bit more grayish, and there will be a thick border at the left side.

Another thing that was added is a limiter for image sizes on posts. Now it doesn't matter how big the image is, it won't be displayed bigger than 940px. This way you won't have pictures flowing out of the post.

That's all for today. The next thing on my list is to [implement pagination](https://github.com/nathanpc/tinymd/issues/1) to the main screen.