# TODO List (October 28, 2012)

I'll try to make this a common post here on the official blog. These posts are going to keep you informed about the planned features for tinymd and the status of things that are currently under development.

Here's the TODO list:

 - Add a commenting system ([Disqus](http://disqus.com/))
 - Polish the interface and the code a little bit more
 - Implement the "Love" feature
 - Create a better way to sort the posts list

I would like to describe some of the changes a little bit. Starting with the "Love" feature: If you notice there's a "Love" link at the right side of the "Permalink", currently that link does exactly nothing, but after implemented this will be a very convenient way for your readers to say that they liked your post, just like Tumblr does, but without having to login, this way you'll be able to know what your readers like to read on your blog.

The second feature that I want to talk about is the sorting one. At the time the `helpers/articles.php` script just get the list of the files inside the `posts` directory and sort them by the date they were modified. I want to improve this by creating something like a cache which will only store the name of the file and the position that it should be shown so when you edit a post it won't go to the top of the index page.

Those are the first ones, if you have any other ideas please feel free to submit a [Issue](https://github.com/nathanpc/tinymd/issues) on the [GitHub page](https://github.com/nathanpc/tinymd) so I can add to the list, and start working on it.