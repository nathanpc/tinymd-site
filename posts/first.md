# First Post

This is the first post on a **tinymd** blog.

The main goal of **tinymd** is to be able to create a extremely simple, yet extremely powerful, blog engine using PHP that doesn't need a database and uses Markdown to format the posts.

I'll be updating this blog with the latest updates and with other stuff related to the project. I hope you like it and **don't forget to send me suggestions via the [GitHub page](https://github.com/nathanpc/tinymd)** (using the [Issues](https://github.com/nathanpc/tinymd/issues) feature).

I'm going to move my current [Tumblr blog](http://nathancampos.me) to a awesome new **tinymd** blog so I can really test it and improve it based on the needs I find while using it for my main blog. I'll try to create importers to make things easier for people that want to migrate from bloated blogs like Tumblr to simpler and fast ones like **tinymd**.