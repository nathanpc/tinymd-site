# Updates (October 29, 2012)

So, yesterday I've published the TODO list. Today I want to share with you the latest things that were added to tinymd:

 - Added a commenting system
 - Added tracking support

Disqus is now supported for comments, so just to go your `config/details.php` file enable Disqus and put your ID (which you can get from the Disqus Control Panel) there. Instantaneously you'll get comments working perfectly. Thanks Disqus for making such a awesome and integrated system.

About the tracking support, tinymd now supports two analytics systems: [Gaug.es](http://gaug.es/) and [Google Analytics](http://google.com/analytics/). Both can be enabled via the well known `config/details.php` file and all you need is the tracking IDs.

So there you go! Now I'm going to work on the improvements to the sorting system, if you have any other ideas please feel free to submit a [Issue](https://github.com/nathanpc/tinymd/issues) on the [GitHub page](https://github.com/nathanpc/tinymd) so I can add to the TODO list, and start working on it.