# Updates (October 30, 2012)

This update is extremely important for one reason. This is the update that improved the post sorting by creating a cache. What this means? This means that now you can edit your files freely and these changes won't change the sorting order on the main (index) page.

Before this update the sorting was done by the Modified Date that every post file had, which means that if you edited that file for some reason it would climb to the top of the posts list, since it was modified recently. Now the script will cache the list of posts on a file called `list_cache.lst` and will only change this file when a completely new file appears at the posts folder by appending that to the cache.


## TODO

During the tests for the caching feature I realized that there's a massive problem on the way that tinymd handles the *permalinks*. Right now it just gets the number of the post by it's position on the index page, which means this links will change every time a new post is added.

I'm going to correct this by using the cache position as the key for the permalinks. I'll try to do this today.


## Update

***Permalinks* problem: Fixed!**

30 minutes after this post I've updated tinymd with the latest fix, so now all your *permalinks* are fixed and won't change after new posts.